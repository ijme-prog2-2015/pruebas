#TIPO ABSTRACTO DE DATO: PUNTO 

from math import sqrt
class punto:
    x = float()
    y = float()


def dist(pa, pb):
    return sqrt ((pa.x - pb.x)**2 + (pa.y - pb.y)**2)


def mover(p, dx, dy):
    p.x = p.x + dx
    p.y = p.y + dy

def mostrar (p):
    print('(', p.x, ',' ,p.y, ')')

#----------------------------------------------
p1 = punto()
p1.x = 2
p1.y = 2

p2= punto()
p2.x = 5
p2.y = 5

hip = dist(p1, p2)
print ('la distancia es: ', hip)


if (hip > 2):
    print ('Es mayor')
else:
    print('Es menor')

